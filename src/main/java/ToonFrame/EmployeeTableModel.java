/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ToonFrame;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import model.Employee;

/**
 *
 * @author ASUS
 */
class EmployeeTableModel extends AbstractTableModel {

    private ArrayList<Employee> data;
    private String[] colName = {"ID", "Name", "LastName", "Telephone", "Salary"};

    public EmployeeTableModel(ArrayList<Employee> data) {
        this.data = data;
    }

    public int getRowCount() {
        return this.data.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Employee emp = this.data.get(rowIndex);
        if (columnIndex == 0) {
            return emp.getId();
        }
        if (columnIndex == 1) {
            return emp.getName();
        }
        if (columnIndex == 2) {
            return emp.getLname();
        }
        if (columnIndex == 3) {
            return emp.getTel();
        }
        if (columnIndex == 4) {
            return emp.getSalary();
        }
        return "";
    }

    @Override
    public String getColumnName(int column) {
        return colName[column];
    }

}
