/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author LENOVO
 */
public class OrderDetail {
    private int id;
    private Order order;
    private Product product;
    private int amount;
    private double price;

    public OrderDetail(int id, Order order, Product product, int amount, double price) {
        this.id = id;
        this.order = order;
        this.product = product;
        this.amount = amount;
        this.price = price;
    }

    public OrderDetail(Order order, Product product, int amount, double price) {
        this(-1,order,product,amount,price);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    
    public  void  addAmount(int amount){
        this.amount = this.amount+amount;
    }
    public  double getTotal(){
        return amount*price;
    }

    @Override
    public String toString() {
        return "OrderDetail{" + "id=" + id + ", order=" + order + ", product=" + product + ", amount=" + amount + ", price=" + price + '}';
    }

    
}
