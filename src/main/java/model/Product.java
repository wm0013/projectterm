/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import dao.ProductDao;
import java.util.ArrayList;

/**
 *
 * @author user
 */
public class Product {
    private int id;
    private String name;
    private double price;
    private String images;
    private int amount;

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Product(int id, String name, double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public Product(int id, String name, double price, String images) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.images = images;
    }
    
    public Product(int id, String name, double price, String images, int amount) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.images = images;
        this.amount = amount;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    @Override
    public String toString() {
        return id+" "+name+" "+price+" "+images+" "+amount;
    }
    
    public static ArrayList<Product> genProductList(ProductDao dao){
        ArrayList<Product> list = dao.getAllImages();
        return list;
    }
    
}
