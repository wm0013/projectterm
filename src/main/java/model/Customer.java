/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author LENOVO
 */
public class Customer {
    private int cusId;
    private String cusName;
    private String cusLastname;
    private String cusTel;

    public Customer(int cusId, String cusName, String cusLastname, String cusTel) {
        this.cusId = cusId;
        this.cusName = cusName;
        this.cusLastname = cusLastname;
        this.cusTel = cusTel;
    }
    public Customer() {
        
    }

    public Customer(String cusName, String cusLastname, String cusTel) {
        this(-1,cusName,cusLastname,cusTel);
    }

    public Customer(int cusId, String cusName, String cusTel) {
        this.cusId = cusId;
        this.cusName = cusName;
        this.cusTel = cusTel;
    }
    
    public Customer(String name, String tel) {
        this(-1, name, tel);
    }

    public Customer(String text) {
        this.cusId=Integer.parseInt(text);
    }
     
    public Customer(int cusId) {
        this.cusId=cusId;
    }
    
    public int getCusId() {
        return cusId;
    }

    public void setCusId(int cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCusLastname() {
        return cusLastname;
    }

    public void setCusLastname(String cusLastname) {
        this.cusLastname = cusLastname;
    }

    public String getCusTel() {
        return cusTel;
    }

    public void setCusTel(String cusTel) {
        this.cusTel = cusTel;
    }

    @Override
    public String toString() {
        return  "CustomerID: "+cusId+ " CustomerName: " + cusName + " CustomerLastName: " + cusLastname  + " CustomerTelephone: " + cusTel;
    }
    
}
