/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ArmFrame;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import model.Customer;

/**
 *
 * @author LENOVO
 */
public class CustomerTableModel extends AbstractTableModel{
    private ArrayList<Customer> customer;
    private Customer cus;
    private String[] colCus = {"ID","Name","LastName","Tel"};

    public CustomerTableModel(ArrayList<Customer> customer) {
        this.customer = customer;
    }
    public CustomerTableModel(Customer customer) {
        this.cus =customer;
    }
     
    @Override
    public int getRowCount() {
        if(cus != null){
            return  1;
        }
        return this.customer.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if(cus != null){
            if (columnIndex == 0) {
            return cus.getCusId();
        }
        if (columnIndex == 1) {
            return cus.getCusName();
        }
        if (columnIndex == 2) {
            return cus.getCusLastname();
        }
        if (columnIndex == 3) {
            return cus.getCusTel();
        }}else{
            Customer cusList = this.customer.get(rowIndex);
        if (columnIndex == 0) {
            return cusList.getCusId();
        }
        if (columnIndex == 1) {
            return cusList.getCusName();
        }
        if (columnIndex == 2) {
            return cusList.getCusLastname();
        }
        if (columnIndex == 3) {
            return cusList.getCusTel();
        }}
        
       
        return "";
    }
    
    @Override
    public String getColumnName(int column) {
        return colCus[column];
    }
    
}
